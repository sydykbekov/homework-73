const express = require('express');
const app = express();
const port = 8000;
const Vigenere = require('caesar-salad').Vigenere;

const password = 'abrakadabra';

app.get('/', (req, res) => {
    res.send('Hello, world!');
});

app.get('/encode/:word', (req, res) => {
    let word = Vigenere.Cipher(password).crypt(req.params.word);
    res.send(word);
});

app.get('/decode/:word', (req, res) => {
    let word = Vigenere.Decipher(password).crypt(req.params.word);
    res.send(word);
});

app.listen(port, () => {
    console.log('We are live on ' + port);
});