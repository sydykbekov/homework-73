const express = require('express');
const app = express();
const port = 8000;

app.get('/', (req, res) => {
    res.send('Hello, world!');
});

app.get('/:word', (req, res) => {
    console.log(req.params);
    res.send(req.params.word);
});

app.listen(port, () => {
    console.log('We are live on ' + port);
});